
WITH
  source AS (
  SELECT
    *
  FROM
  {{ source('open_positions', 'users_country') }}),
  renamed AS (
    SELECT
      *
    FROM
      source)
SELECT
  *
FROM
  renamed
