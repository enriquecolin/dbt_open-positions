{% docs description_users_country %}

## Users Country - Staging Table

### About this table

This table contains all emails of Ebury's employees, with their Salesforce IDs, their respective countries and roles, as well as with their first, and last names and a field indicating whether they are active users in salesforce (IsActive = 1) or not active users in salesforce (IsActive = 0).

### Data Sources

This table is sourced from:
  - sources_for_shared

{% enddocs %}
