
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('open_positions', 'net_open_position')}}),
  renamed AS (
    SELECT
      *
    FROM
      source)
SELECT
  *
FROM
  renamed
