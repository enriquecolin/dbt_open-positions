
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('open_positions', 'dynamic_open_position')}}),
  renamed AS (
    SELECT
      *
    FROM
      source)
SELECT
  *
FROM
  renamed
