
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('open_positions', 'classic_open_position')}}),
  renamed AS (
    SELECT
      *
    FROM
      source)
SELECT
  *
FROM
  renamed
