{% docs description_stg_net %}

## Net Credit Conditions - Staging Table

### About this table

Table containing all open contracts from BOS that fall under the Net Credit Conditions.

### Business Insights

Under this type of Credit Condition, the exposure is evaluated on an aggregate level, i.e. all open positions of a Client are evaluated as a whole. The maximum Exposure Level and Margin Call Amount are based on absolute values. With Dynamic and Net Credit Conditions, it is less often that a Client gets Margin Called, as opposed to the Classic Credit Conditions, where it happens much more often.

### Data Sources

This table is sourced from:
  - open_positions

{% enddocs %}
