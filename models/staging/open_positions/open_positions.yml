
version: 2

models:
  - name: stg_open_positions__classic_open_position
    description: '{{ doc("description_stg_classic") }}'

    columns:
      - name: Deal
        description: Transaction Receipt from BOS
        tests:
          - unique
          - not_null

      - name: Dealer_Name
        description: Dealer's First Name
        tests:
          - not_null

      - name: Dealer_Surname
        description: Dealer's Surname
        tests:
          - not_null

      - name: Client_ID
        description: Client's ID Number
        tests:
          - not_null

      - name: Client_Name
        description: Full Client's Name
        tests:
          - not_null

      - name: Type
        description: Type of Client
        tests:
          - not_null
          - accepted_values:
              values: ['Company', 'Individual']

      - name: Order_Date
        description: Booking date of the Deal
        tests:
          - not_null

      - name: Maturity_Date
        description: Expiration date of the Deal
        tests:
          - not_null

      - name: Sell_Ccy
        description: Selling Currency
        tests:
          - not_null

      - name: Sell_Amount
        description: Selling Amount
        tests:
          - not_null

      - name: Buy_Ccy
        description: Buying Currency
        tests:
          - not_null

      - name: Buy_Amount
        description: Buying Amount
        tests:
          - not_null

      - name: Buy_Balance
        description: none
        tests:
          - not_null

      - name: PL_Ccy
        description: Deal's Exposure Currency
        tests:
          - not_null

      - name: PL_Amount
        description: Exposure of Deal without taking into account the Current Deposit Amount
        tests:
          - not_null

      - name: Exposure_Ccy
        description: Exposure's Currency
        tests:
          - not_null

      - name: Exposure_Amount
        description: Exposure of Deal taking into account the Current Deposit Amount
        tests:
          - not_null

      - name: Rates_Live_Rate
        description: none
        tests:
          - not_null

      - name: Rates_Trigger_Rate
        description: none
        tests:
          - not_null

      - name: Levels_Variation_Margin
        description: Variation Margin (%) is the maximum exposure allowed before issuing a Margin Call
        tests:
          - not_null

      - name: Levels_Live_Variation
        description: Variation Margin (%) where the deal's exposure is at the moment (if above Levels_Variation_Margin then Margin Call is issued)
        tests:
          - not_null

      - name: Current_Deposit_Percentage
        description: Percentage of Current Deposit held
        tests:
          - not_null

      - name: Current_Deposit_Ccy
        description: Current Deposit's Currency
        tests:
          - not_null

      - name: Current_Deposit_Amount
        description: Current Deposit Amount
        tests:
          - not_null

      - name: Current_MC_Held_Percentage
        description: Current Margin Call held (%)
        tests:
          - not_null

      - name: Current_MC_Held_Ccy
        description: Current Margin Call held Currency
        tests:
          - not_null

      - name: Current_MC_Held_Amount
        description: Current Margin Call Amount held
        tests:
          - not_null

      - name: Margin_Call_Ccy
        description: Margin Call Currency
        tests:
          - not_null

      - name: Margin_Call_Amount
        description: Amount received after issuing a MC (when Exposure Amount exceeds the agreed Variation Margin, i.e. Client is OOTM)
        tests:
          - not_null

      - name: Margin_Call_Amount_To_Refund
        description: Margin Call Amount To Refund the Client if he has gone back ITM
        tests:
          - not_null

      - name: Overdue_Deposit_Ccy
        description: Overdue Deposit's Currency
        tests:
          - not_null

      - name: Overdue_Deposit_Amount
        description: Deposit Amount requested by Ebury that is still pending
        tests:
          - not_null

      - name: Overdue_MC_Ccy
        description: Overdue Margin Call Currency
        tests:
          - not_null

      - name: Overdue_MC_Amount
        description: Overdue Amount requested by Ebury that is still pending after MC is issued
        tests:
          - not_null

      - name: Spread
        description: Spread charged
        tests:
          - not_null

      - name: Credit_Condition
        description: Client's Risk
        tests:
          - not_null
          - accepted_values:
              values: ['In Risk', 'Not Risk']



  - name: stg_open_positions__dynamic_open_position
    description: '{{ doc("description_stg_dynamic") }}'
    columns:
      - name: Dealer_Name
        description: Dealer's First Name
        tests:
          - not_null

      - name: Dealer_Surname
        description: Dealer's Surname
        tests:
          - not_null

      - name: Client_ID
        description: Client's ID Number
        tests:
          - not_null

      - name: Client_Name
        description: Full Client's Name

      - name: Type
        description: Type of Client
        tests:
          - not_null
          - accepted_values:
              values: ['Company', 'Individual']

      - name: Line_Approved_Ccy
        description: Approved Line's Currency
        tests:
          - not_null

      - name: Line_Approved_Amount
        description: Approved Line Amount for the Client
        tests:
          - not_null

      - name: Line_Used_Ccy
        description: Currency of Line Used
        tests:
          - not_null

      - name: Line_Used_Amount
        description: Amount of the Line Approved that has been utilised
        tests:
          - not_null

      - name: Aggregated_PL_Ccy
        description: Currency of Aggregated P&L (MtM)
        tests:
          - not_null

      - name: Aggregated_PL_Amount
        description: Aggregated P&L Amount (MtM; total exposure without taking into account Initial Deposit and Margin Call that have been paid)
        tests:
          - not_null

      - name: Exposure_Total_Ccy
        description: Currency of Total Exposure
        tests:
          - not_null

      - name: Exposure_Total_Amount
        description: Total Exposure Amount of the aggregated contracts (taking into account the Initial Deposit and Margin Call that have been paid)
        tests:
          - not_null

      - name: Exposure_Margin_Call_Ccy
        description: Margin Call Exposure Currency
        tests:
          - not_null

      - name: Exposure_Margin_Call_Amount
        description: Margin Call Exposure Amount (if this amount goes above the Variation Margin amount, the Client is in need of getting MC)
        tests:
          - not_null

      - name: Variation_Margin_Ccy
        description: Variation Margin's Currency
        tests:
          - not_null

      - name: Variation_Margin_Amount
        description: Variation Margin Amount is the maximum exposure allowed before issuing a MC (based on percentage of Line Used)
        tests:
          - not_null

      - name: Margin_Call_Ccy
        description: Margin Call Currency
        tests:
          - not_null

      - name: Margin_Call_Amount
        description: Margin Call Amount requested by Ebury when Exposure is above Variation Margin (based on percentage of Line Used)
        tests:
          - not_null

      - name: Margin_Call_Amount_To_Refund
        description: Margin Call Amount To Refund the Client if he has gone back ITM
        tests:
          - not_null

      - name: Current_Deposit_Held_Ccy
        description: Current Deposit Held's Currency
        tests:
          - not_null

      - name: Current_Deposit_Held_Amount
        description: Current Deposit Amount Held to cover for any unforeseen credit events during the contract's life
        tests:
          - not_null

      - name: Current_MC_Held_Ccy
        description: Current MC Held Currency
        tests:
          - not_null

      - name: Current_MC_Held_Amount
        description: Current MC Held Amount
        tests:
          - not_null

      - name: Overdue_Deposit_Ccy
        description: Overdue Deposit's Currency
        tests:
          - not_null

      - name: Overdue_Deposit_Amount
        description: Deposit Amount that is still pending
        tests:
          - not_null

      - name: Overdue_MC_Ccy
        description: Overdue Margin Call's Currency
        tests:
          - not_null

      - name: Overdue_MC_Amount
        description: Margin Call Amount still pending after MC is issued
        tests:
          - not_null

      - name: Credit_Condition
        description: Client's Risk
        tests:
          - not_null
          - accepted_values:
              values: ['In Risk', 'Semi Risk', 'Not Risk']



  - name: stg_open_positions__net_open_position
    description: '{{ doc("description_stg_net") }}'
    columns:
      - name: Dealer_Name
        description: Dealer's First Name
        tests:
          - not_null

      - name: Dealer_Surname
        description: Dealer's Surname
        tests:
          - not_null

      - name: Client_ID
        description: Client's ID Number
        tests:
          - not_null

      - name: Client_Name
        description: Full Client's Name

      - name: Type
        description: Type of Client
        tests:
          - not_null
          - accepted_values:
              values: ['Company', 'Individual']

      - name: Line_Approved_Ccy
        description: Approved Line's Currency
        tests:
          - not_null

      - name: Line_Approved_Amount
        description: Approved Line Amount for the Client
        tests:
          - not_null

      - name: Line_Used_Ccy
        description: Currency of Line Used
        tests:
          - not_null

      - name: Line_Used_Amount
        description: Amount of the Line Approved that has been utilised
        tests:
          - not_null

      - name: Aggregated_PL_Ccy
        description: Currency of Aggregated P&L (MtM)
        tests:
          - not_null

      - name: Aggregated_PL_Amount
        description: Aggregated P&L Amount (MtM)
        tests:
          - not_null

      - name: Exposure_Total_Ccy
        description: Currency of Total Exposure
        tests:
          - not_null

      - name: Exposure_Total_Amount
        description: Total Exposure Amount of the aggregated contracts
        tests:
          - not_null

      - name: Exposure_Margin_Call_Ccy
        description: Margin Call Exposure Currency
        tests:
          - not_null

      - name: Exposure_Margin_Call_Amount
        description: Margin Call Exposure Amount (if this amount goes above the Variation Margin amount, the Client is in need of getting MC)
        tests:
          - not_null

      - name: Variation_Margin_Ccy
        description: Variation Margin's Currency
        tests:
          - not_null

      - name: Variation_Margin_Amount
        description: Variation Margin Amount is the maximum exposure allowed before issuing a MC
        tests:
          - not_null

      - name: Margin_Call_Ccy
        description: Margin Call Currency
        tests:
          - not_null

      - name: Margin_Call_Amount
        description: Margin Call Amount requested by Ebury when Exposure is above Variation Margin
        tests:
          - not_null

      - name: Margin_Call_Amount_To_Refund
        description: Margin Call Amount To Refund the Client if he has gone back ITM
        tests:
          - not_null

      - name: Current_Deposit_Held_Ccy
        description: Current Deposit Held's Currency
        tests:
          - not_null

      - name: Current_Deposit_Held_Amount
        description: Current Deposit Amount Held to cover for any unforeseen credit events during the contract's life
        tests:
          - not_null

      - name: Current_MC_Held_Ccy
        description: Current MC Held Currency
        tests:
          - not_null

      - name: Current_MC_Held_Amount
        description: Current MC Held Amount
        tests:
          - not_null

      - name: Overdue_Deposit_Ccy
        description: Overdue Deposit's Currency
        tests:
          - not_null

      - name: Overdue_Deposit_Amount
        description: Deposit Amount that is still pending
        tests:
          - not_null

      - name: Overdue_MC_Ccy
        description: Overdue Margin Call's Currency
        tests:
          - not_null

      - name: Overdue_MC_Amount
        description: Margin Call Amount still pending after MC is issued
        tests:
          - not_null

      - name: Credit_Condition
        description: Client's Risk
        tests:
          - not_null
          - accepted_values:
              values: ['In Risk', 'Semi Risk', 'Not Risk']

sources:
  - name: open_positions
    description: 'n/a'

    tables:
      - name: classic_open_position
      - name: dynamic_open_position
      - name: net_open_position
