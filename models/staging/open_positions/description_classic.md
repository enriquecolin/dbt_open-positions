{% docs description_stg_classic %}

## Classic Credit Conditions - Staging Table

### About this table

Table containing all open contracts from BOS that fall under the Classic Credit Conditions.

### Business Insights

Under this type of Credit Condition, the exposure is evaluated on a deal by deal basis, trigerring a Margin Call to the Client much more often than if it were evaluated on an aggregate level, as with the Net and Dynamic Credit Conditions.

### Data Sources

This table is sourced from:
  - open_positions

{% enddocs %}
