{% docs net_op_final_table %}

## Net Credit Conditions - Final Table

### About this table
This table contains all Clients' open positions that fall under the Net Credit Conditions, including the Dealers and Country Managers for each account.

As a summary, this table contains the corrected Dealer names, their assigned Clients' open positions, and their respective country managers (filtering by those who are active and whose role is Country Manager).

For each Client, there is an entry that points to the dealer's email, and another entry which is identical except that it points to the country manager's email. The link between Dealers and CMs is done through the country key, to finally arrive to the net_open_positions table.

### Business Insights

For the Net Credit Conditions, the exposure is evaluated on an aggregate level. The maximum Exposure Level and MC Amount are based on absolute values.

Net and Dynamic Open Positions tables will have the same columns, because they calculate the exposure by aggregating all open positions contracts for a Client, whereas the Classic Open Positions table will have different columns, such as "Deal", due to it being a type of credit condition that evaluates exposure on a deal by deal basis (which triggers a Margin Call much more often).

### Data Sources

This table is sourced from:
  - net_open_positions_with_dealer
  - active_country_managers


{% enddocs %}
