{% docs classic_op_final_table %}

## Classic Credit Conditions - Final Table

### About this table

This table contains all Deals that fall under the Classic Credit Conditions including the Dealers and Country Managers for each Deal.
As a summary, this table contains the corrected Dealer names, their assigned deals (only selecting those that belong to Ebury = EBPB or EBPO), and their respective country managers (filtering by those who are active and whose role is Country Manager).

For each Deal, there is an entry that points to the dealer's email, and another entry which is identical except that it points to the country manager's email. The link between Dealers and CMs is done through the country key, to finally arrive to the classic_open_positions table.

### Business Insights

Net and Dynamic Open Positions tables will have the same columns, because they calculate the exposure by aggregating all open positions contracts for a Client, whereas the Classic Open Positions table will have different columns, such as "Deal", due to it being a type of credit condition that evaluates exposure on a deal by deal basis (which triggers a Margin Call much more often).

### Data Sources

This table is sourced from:
  - classic_open_positions_with_dealer
  - active_country_managers

{% enddocs %}
