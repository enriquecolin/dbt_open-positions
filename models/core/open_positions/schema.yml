version: 2

models:
  - name: classic_open_positions
    description: '{{ doc("classic_op_final_table") }}'

    columns:
        - name: Deal
          description: Transaction Receipt from BOS
          tests:
            - not_null

        - name: Dealer_Name
          description: Dealer's First Name
          tests:
            - not_null

        - name: Dealer_Surname
          description: Dealer's Surname
          tests:
            - not_null

        - name: Client_ID
          description: Client's ID Number
          tests:
            - not_null

        - name: Client_Name
          description: Full Client's Name
          tests:
            - not_null

        - name: Type
          description: Type of Client
          tests:
            - not_null
            - accepted_values:
                values: ["Company", "Individual"]

        - name: Order_Date
          description: Booking date of the Deal
          tests:
            - not_null

        - name: Maturity_Date
          description: Expiration date of the Deal
          tests:
            - not_null

        - name: Sell_Ccy
          description: Selling Currency
          tests:
            - not_null

        - name: Sell_Amount
          description: Selling Amount
          tests:
            - not_null

        - name: Buy_Ccy
          description: Buying Currency
          tests:
            - not_null

        - name: Buy_Amount
          description: Buying Amount
          tests:
            - not_null

        - name: Buy_Balance
          description: Description pending
          tests:
            - not_null

        - name: PL_Ccy
          description: Deal's Exposure Currency
          tests:
            - not_null

        - name: PL_Amount
          description: Exposure of Deal without taking into account the Current Deposit Amount
          tests:
            - not_null

        - name: Exposure_Ccy
          description: Exposure's Currency
          tests:
            - not_null

        - name: Exposure_Amount
          description: Exposure of Deal taking into account the Current Deposit Amount
          tests:
            - not_null

        - name: Rates_Live_Rate
          description: Description pending
          tests:
            - not_null

        - name: Rates_Trigger_Rate
          description: Description pending
          tests:
            - not_null

        - name: Levels_Variation_Margin
          description: Variation Margin as a percentage is the maximum exposure allowed before issuing a Margin Call
          tests:
            - not_null

        - name: Levels_Live_Variation
          description: Variation Margin as a percentage where the deal's exposure is at the moment (if above Levels_Variation_Margin then Margin Call is issued)
          tests:
            - not_null

        - name: Current_Deposit_Percentage
          description: Percentage of Current Deposit held
          tests:
            - not_null

        - name: Current_Deposit_Ccy
          description: Current Deposit's Currency
          tests:
            - not_null

        - name: Current_Deposit_Amount
          description: Current Deposit Amount
          tests:
            - not_null

        - name: Current_MC_Held_Percentage
          description: Current Margin Call held as a percentage
          tests:
            - not_null

        - name: Current_MC_Held_Ccy
          description: Current Margin Call held Currency
          tests:
            - not_null

        - name: Current_MC_Held_Amount
          description: Current Margin Call Amount held
          tests:
            - not_null

        - name: Margin_Call_Ccy
          description: Margin Call Currency
          tests:
            - not_null

        - name: Margin_Call_Amount
          description: Amount received after issuing a MC (when Exposure Amount exceeds the agreed Variation Margin, i.e. Client is OOTM)
          tests:
            - not_null

        - name: Margin_Call_Amount_To_Refund
          description: Margin Call Amount To Refund the Client if he has gone back ITM
          tests:
            - not_null

        - name: Overdue_Deposit_Ccy
          description: Overdue Deposit's Currency
          tests:
            - not_null

        - name: Overdue_Deposit_Amount
          description: Deposit Amount requested by Ebury that is still pending
          tests:
            - not_null

        - name: Overdue_MC_Ccy
          description: Overdue Margin Call Currency
          tests:
            - not_null

        - name: Overdue_MC_Amount
          description: Overdue Amount requested by Ebury that is still pending after MC is issued
          tests:
            - not_null

        - name: Spread
          description: Spread charged
          tests:
            - not_null

        - name: Credit_Condition
          description: Client's Risk (more information on how this is assessed can be found in the following link https://www.notion.so/eburydata/BOS-Risk-Analysis-15d6e73c4e514ad3b7bf8d9847eb87b7)
          tests:
            - not_null
            - accepted_values:
                values: ["In Risk", "Not Risk"]

        - name: Surname
          description: Dealer's Surname (as it appears in users_country)
          tests:
            - not_null

        - name: Name
          description: Dealer's First Name (as it appears in users_country)
          tests:
            - not_null

        - name: country
          description: Country where Dealer and Country Manager are based


        - name: username
          description: Dealer's email or Country Manager's email (1 entry for a Dealer, 1 entry for Dealer's assigned CM)



  - name: dynamic_open_positions
    description: '{{ doc("dynamic_op_final_table") }}'

    columns:
        - name: Dealer_Name
          description: Dealer's First Name
          tests:
            - not_null

        - name: Dealer_Surname
          description: Dealer's Surname
          tests:
            - not_null

        - name: Client_ID
          description: Client's ID Number
          tests:
            - not_null

        - name: Client_Name
          description: Full Client's Name
          tests:
            - not_null

        - name: Type
          description: Type of Client
          tests:
            - not_null
            - accepted_values:
                values: ["Company", "Individual"]

        - name: Line_Approved_Ccy
          description: Approved Line's Currency
          tests:
            - not_null

        - name: Line_Approved_Amount
          description: Approved Line Amount for the Client
          tests:
            - not_null

        - name: Line_Used_Ccy
          description: Currency of Line Used
          tests:
            - not_null

        - name: Line_Used_Amount
          description: Amount of the Line Approved that has been utilised
          tests:
            - not_null

        - name: Aggregated_PL_Ccy
          description: Currency of Aggregated P&L (MtM)
          tests:
            - not_null

        - name: Aggregated_PL_Amount
          description: Aggregated P&L Amount (MtM; total exposure without taking into account Initial Deposit and Margin Call that have been paid)
          tests:
            - not_null

        - name: Exposure_Total_Ccy
          description: Currency of Total Exposure
          tests:
            - not_null

        - name: Exposure_Total_Amount
          description: Total Exposure Amount of the aggregated contracts (taking into account the Initial Deposit and Margin Call that have been paid)
          tests:
            - not_null

        - name: Exposure_Margin_Call_Ccy
          description: Margin Call Exposure Currency
          tests:
            - not_null

        - name: Exposure_Margin_Call_Amount
          description: Margin Call Exposure Amount (if this amount goes above the Variation Margin amount, the Client is in need of getting MC)
          tests:
            - not_null

        - name: Variation_Margin_Ccy
          description: Variation Margin's Currency
          tests:
            - not_null

        - name: Variation_Margin_Amount
          description: Variation Margin Amount is the maximum exposure allowed before issuing a MC (based on percentage of Line Used)
          tests:
            - not_null

        - name: Margin_Call_Ccy
          description: Margin Call Currency
          tests:
            - not_null

        - name: Margin_Call_Amount
          description: Margin Call Amount requested by Ebury when Exposure is above Variation Margin, i.e. Client is OOTM (based on percentage of Line Used)
          tests:
            - not_null

        - name: Margin_Call_Amount_To_Refund
          description: Margin Call Amount To Refund the Client if he has gone back ITM
          tests:
            - not_null

        - name: Current_Deposit_Held_Ccy
          description: Current Deposit Held's Currency
          tests:
            - not_null

        - name: Current_Deposit_Held_Amount
          description: Current Deposit Amount Held to cover for any unforeseen credit events during the contract's life
          tests:
            - not_null

        - name: Current_MC_Held_Ccy
          description: Current MC Held Currency
          tests:
            - not_null

        - name: Current_MC_Held_Amount
          description: Current MC Held Amount
          tests:
            - not_null

        - name: Overdue_Deposit_Ccy
          description: Overdue Deposit's Currency
          tests:
            - not_null

        - name: Overdue_Deposit_Amount
          description: Deposit Amount that is still pending
          tests:
            - not_null

        - name: Overdue_MC_Ccy
          description: Overdue Margin Call's Currency
          tests:
            - not_null

        - name: Overdue_MC_Amount
          description: Margin Call Amount still pending after MC is issued
          tests:
            - not_null

        - name: Credit_Condition
          description: Client's Risk (more information on how this is assessed can be found in the following link https://www.notion.so/eburydata/BOS-Risk-Analysis-15d6e73c4e514ad3b7bf8d9847eb87b7)
          tests:
            - not_null
            - accepted_values:
                values: ["In Risk", "Semi Risk", "Not Risk"]

        - name: Surname
          description: Dealer's Surname (as it appears in users_country)
          tests:
            - not_null

        - name: Name
          description: Dealer's First Name (as it appears in users_country)
          tests:
            - not_null

        - name: country
          description: Country where Dealer and Country Manager are based


        - name: username
          description: Dealer's email or Country Manager's email (1 entry for a Dealer, 1 entry for Dealer's assigned CM)



  - name: net_open_positions
    description: '{{ doc("net_op_final_table") }}'

    columns:
        - name: Dealer_Name
          description: Dealer's First Name
          tests:
            - not_null

        - name: Dealer_Surname
          description: Dealer's Surname
          tests:
            - not_null

        - name: Client_ID
          description: Client's ID Number
          tests:
            - not_null

        - name: Client_Name
          description: Full Client's Name

        - name: Type
          description: Type of Client
          tests:
            - not_null
            - accepted_values:
                values: ["Company", "Individual"]

        - name: Line_Approved_Ccy
          description: Approved Line's Currency
          tests:
            - not_null

        - name: Line_Approved_Amount
          description: Approved Line Amount for the Client
          tests:
            - not_null

        - name: Line_Used_Ccy
          description: Currency of Line Used
          tests:
            - not_null

        - name: Line_Used_Amount
          description: Amount of the Line Approved that has been utilised
          tests:
            - not_null

        - name: Aggregated_PL_Ccy
          description: Currency of Aggregated P&L (MtM)
          tests:
            - not_null

        - name: Aggregated_PL_Amount
          description: Aggregated P&L Amount (MtM)
          tests:
            - not_null

        - name: Exposure_Total_Ccy
          description: Currency of Total Exposure
          tests:
            - not_null

        - name: Exposure_Total_Amount
          description: Total Exposure Amount of the aggregated contracts
          tests:
            - not_null

        - name: Exposure_Margin_Call_Ccy
          description: Margin Call Exposure Currency
          tests:
            - not_null

        - name: Exposure_Margin_Call_Amount
          description: Margin Call Exposure Amount (if this amount goes above the Variation Margin amount, the Client is in need of getting MC)
          tests:
            - not_null

        - name: Variation_Margin_Ccy
          description: Variation Margin's Currency
          tests:
            - not_null

        - name: Variation_Margin_Amount
          description: Variation Margin Amount is the maximum exposure allowed before issuing a MC
          tests:
            - not_null

        - name: Margin_Call_Ccy
          description: Margin Call Currency
          tests:
            - not_null

        - name: Margin_Call_Amount
          description:  Margin Call Amount requested by Ebury when Exposure is above Variation Margin, i.e. Client is OOTM (calculated in absolute values)
          tests:
            - not_null

        - name: Margin_Call_Amount_To_Refund
          description: Margin Call Amount To Refund the Client if he has gone back ITM
          tests:
            - not_null

        - name: Current_Deposit_Held_Ccy
          description: Current Deposit Held's Currency
          tests:
            - not_null

        - name: Current_Deposit_Held_Amount
          description: Current Deposit Amount Held to cover for any unforeseen credit events during the contract's life
          tests:
            - not_null

        - name: Current_MC_Held_Ccy
          description: Current MC Held Currency
          tests:
            - not_null

        - name: Current_MC_Held_Amount
          description: Current MC Held Amount
          tests:
            - not_null

        - name: Overdue_Deposit_Ccy
          description: Overdue Deposit's Currency
          tests:
            - not_null

        - name: Overdue_Deposit_Amount
          description: Deposit Amount that is still pending
          tests:
            - not_null

        - name: Overdue_MC_Ccy
          description: Overdue Margin Call's Currency
          tests:
            - not_null

        - name: Overdue_MC_Amount
          description: Margin Call Amount still pending after MC is issued
          tests:
            - not_null

        - name: Credit_Condition
          description: Client's Risk (more information on how this is assessed can be found in the following link https://www.notion.so/eburydata/BOS-Risk-Analysis-15d6e73c4e514ad3b7bf8d9847eb87b7)
          tests:
            - not_null
            - accepted_values:
                values: ["In Risk", "Semi Risk", "Not Risk"]

        - name: Surname
          description: Dealer's Surname (as it appears in users_country)
          tests:
            - not_null

        - name: Name
          description: Dealer's First Name (as it appears in users_country)
          tests:
            - not_null

        - name: country
          description: Country where Dealer and Country Manager are based


        - name: username
          description: Dealer's email or Country Manager's email (1 entry for a Dealer, 1 entry for Dealer's assigned CM)
