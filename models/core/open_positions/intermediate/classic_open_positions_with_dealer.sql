
WITH
  classic_open_positions AS(
  SELECT
    *,
    CASE
      WHEN (Dealer_Surname = "Nunez") THEN "Nunez-Villaveiran Blanco"
      WHEN (Dealer_Surname = "Arino") THEN "Arino Sanz"
      WHEN (Dealer_Surname = "Perez") THEN "Pérez"
      WHEN (Dealer_Surname = "Debeir") THEN "De Beir"
      WHEN (Dealer_Surname = "Catalan") THEN "Catalán"
      WHEN (Dealer_Surname = "Derorthays") THEN "De Rorthays"
      WHEN (Dealer_Surname = "Vanderelst") THEN "Van der Elst"
      WHEN (Dealer_Surname = "Vandermeer") THEN "Van der Meer"
      WHEN (Dealer_Surname = "Vanhanegem") THEN "van Hanegem"
      WHEN (Dealer_Surname = "Mcmurray") THEN "McMurray"
    ELSE
    Dealer_Surname
  END
    AS Surname,
    CASE
      WHEN (Dealer_Name = "Josemaria") THEN "Jose Maria"
      WHEN (Dealer_Name = "Tim") THEN "Timothee"
      WHEN (Dealer_Name = "Pe") THEN "Pierre-Emmanuel"
    ELSE
    Dealer_Name
  END
    AS Name
  FROM
    {{ref('stg_open_positions__classic_open_position') }}),
  dealers AS(
    SELECT
      *
    FROM
      {{ref('stg_users')}}
  ),
  final AS (
  SELECT
    classic_open_positions.*,
    dealers.country,
    dealers.username
  FROM
    classic_open_positions
  LEFT JOIN
      dealers
  ON
    CONCAT(dealers.first_name, ' ', dealers.last_name) =
    CONCAT(classic_open_positions.Name, ' ', classic_open_positions.Surname)
  WHERE
    SUBSTR(classic_open_positions.Deal, 1, 4) = "EBPB"
    OR SUBSTR(classic_open_positions.Deal, 1, 4) = "EBPO")
SELECT
  *
FROM
  final
