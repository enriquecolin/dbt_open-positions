
WITH
  net_open_positions AS(
  SELECT
    *,
    CASE
      WHEN (Dealer_Surname = "Nunez") THEN "Nunez-Villaveiran Blanco"
      WHEN (Dealer_Surname = "Arino") THEN "Arino Sanz"
      WHEN (Dealer_Surname = "Perez") THEN "Pérez"
      WHEN (Dealer_Surname = "Debeir") THEN "De Beir"
      WHEN (Dealer_Surname = "Catalan") THEN "Catalán"
      WHEN (Dealer_Surname = "Derorthays") THEN "De Rorthays"
      WHEN (Dealer_Surname = "Vanderelst") THEN "Van der Elst"
      WHEN (Dealer_Surname = "Vandermeer") THEN "Van der Meer"
      WHEN (Dealer_Surname = "Vanhanegem") THEN "van Hanegem"
      WHEN (Dealer_Surname = "Mcmurray") THEN "McMurray"
    ELSE
    Dealer_Surname
  END
    AS Surname,
    CASE
      WHEN (Dealer_Name = "Josemaria") THEN "Jose Maria"
      WHEN (Dealer_Name = "Tim") THEN "Timothee"
      WHEN (Dealer_Name = "Pe") THEN "Pierre-Emmanuel"
    ELSE
    Dealer_Name
  END
    AS Name
  FROM
    {{ref('stg_open_positions__dynamic_open_position') }}),
  dealers AS(
    SELECT
      *
    FROM
      {{ref('stg_users')}}
  ),
  final AS (
  SELECT
    net_open_positions.*,
    dealers.country,
    dealers.username
  FROM
    net_open_positions
  LEFT JOIN
      dealers
  ON
    CONCAT(dealers.first_name, ' ', dealers.last_name) = CONCAT(net_open_positions.Name, ' ', net_open_positions.Surname))
SELECT
  *
FROM
  final
