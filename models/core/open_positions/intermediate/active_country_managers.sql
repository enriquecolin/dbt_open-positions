
WITH
  country_manager AS(
  SELECT
    username,
    country
  FROM
    {{ref('stg_users')}}
  WHERE
    role = 'Country Manager'
    AND IsActive = "1")
SELECT
  *
FROM
  country_manager
