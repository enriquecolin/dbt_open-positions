{% docs description_classic_op_intermediate %}

## Classic Credit Conditions - Intermediate Table

### About this table

This table contains all Deals that fall under the Classic Credit Conditions including the Dealers' countries and emails.
As a summary, this table contains the corrected Dealer names, their assigned deals (only selecting those that belong to Ebury = EBPB or EBPO), and their respective emails and countries. The dealers' first names and surnames have been modified so that they match the exact names found in the users table, since this is the key to join both tables on, to finally arrive to the classic_open_positions_with_dealer table.

### Data Sources

This table is sourced from:
  - stg_open_positions__classic_open_position
  - stg_users


{% enddocs %}
