{% docs description_active_cms %}

## Users Country - Intermediate Table

### About this table

Table containing the emails of all active Country Managers and the countries where they are based.

### Data Sources

This table is sourced from:
  - stg_users

{% enddocs %}
