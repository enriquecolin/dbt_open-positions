{% docs description_dynamic_op_intermediate %}

## Dynamic Credit Conditions - Intermediate Table

### About this table
This table contains all Clients' open positions that fall under the Dynamic Credit Conditions including the Dealer's country and emails for each.
As a summary, this table contains the corrected Dealer names, their assigned Clients' open positions, and their respective emails and countries. The dealers' first names and surnames have been modified so that they match the exact names found in the users table, since this is the key to join both tables on, to finally arrive to the dynamic_open_positions_with_dealer table.

### Data Sources

This table is sourced from:
  - stg_open_positions__dynamic_open_position
  - stg_users


{% enddocs %}
