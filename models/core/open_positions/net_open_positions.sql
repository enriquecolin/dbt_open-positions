
WITH
  open_positions_with_dealer AS(
    SELECT
      *
    FROM
      {{ref('net_open_positions_with_dealer')}}
  ),
  country_manager AS(
    SELECT
      *
    FROM
      {{ref('active_country_managers')}}
  ),
  final AS(
    SELECT
      *
    FROM(
      SELECT
        open_positions_with_dealer.*,
        cm.username
      FROM(
        SELECT
          * EXCEPT(username)
        FROM
          open_positions_with_dealer) open_positions_with_dealer
      LEFT JOIN
        country_manager cm
      ON
        open_positions_with_dealer.country = cm.country
      UNION ALL
      SELECT
        *
      FROM
        open_positions_with_dealer)
      )
SELECT
  *
FROM
  final
